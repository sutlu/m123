# Grundlagen DNS


## Screenshot
![DNS-PDU](../img/01DNS-PDU.png)

- Wie weiss der PC0, dass er den *DNS Query* an `1.1.1.1` bzw. DNS-Server schicken muss?
  - Seine Config zeigt auf den DNS-Server mit der IP 1.1.1.1.
- Welches Layer 4 Protokoll kommt bei DHCP zum Einsatz?
  - UDP
- Was für einen `QDCOUNT` Wert hat es und in welchem Teil ist diese Feld?
  - 1, IM DNS Header
- Wie lautet die Transaction ID?
  - 0x7bcb
- Was für einen `TYPE` und `CLASS` hat der Query?
  - Type 1
  - Class 1
- Was für eine IP Adresse steht in der DNS query response bzw. DNS Answer?
  - 31.31.31.31
  - ![DNS-Answer](../img/02DNS-Answer.png)
- Inwiefern unterscheidet sich die DNS query response zur DNS query? In welchem/n Feld(ern) sind der "DNS-Packet-Typ" festgehalten?
  - Natürlich haben sich Src und Dest IP/Mac Adresse gewechselt
  - Das Length Feld hat sich von 0 auf 4 erhöht.
  - Das Ancount Feld hat sich von 0 auf 1 erhöht.
  - ![DNS-Difference](../img/03DNS-Difference.png)

